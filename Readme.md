#Quick setup
## Create .env file 
```bash
cp .env.dist .env
```
##Start docker containers
```bash
docker-compose up -d
```
##Create database and database structure
```bash
docker-compose exec php bin/console doctrine:database:create
docker-compose exec php bin/console doctrine:schema:create
```
##Verify if app is running 
Navigate in browser to http://localhost:8888/

#Sample post requests
##Create new item
```http request
POST http://localhost:8888/document/
Accept: application/json
Cache-Control: no-cache

{     "name": "Test document",     "description": "Some test document" }

```
##Get list of items
```http request
GET http://localhost:8888/document/
Accept: application/json
Cache-Control: no-cache

```