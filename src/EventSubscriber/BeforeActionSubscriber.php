<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 07.10.2018
 * Time: 09:32
 */

namespace App\EventSubscriber;

use App\RequestToArray\RequestToArrayFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class BeforeActionSubscriber
 * @package App\EventSubscriber
 */
class BeforeActionSubscriber implements EventSubscriberInterface
{
    /**
     * @var RequestToArrayFactory
     */
    private $requestToArray;

    /**
     * BeforeActionSubscriber constructor.
     */
    public function __construct(RequestToArrayFactory $requestToArray)
    {
        $this->requestToArray = $requestToArray;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'convertRequestStringToArray',
        );
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function convertRequestStringToArray(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        if ($request->getContent()) {
            $data = $this->requestToArray::get($request->getRequestFormat())->deserialize($request->getContent());
            $request->request->replace(is_array($data) ? $data : array());
        }
    }
}