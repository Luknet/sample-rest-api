<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 06.10.2018
 * Time: 16:59
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Document
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="document")
 * @ORM\HasLifecycleCallbacks
 */
class Document
{
    /**
     * @var string $id
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    public $id;
    /**
     * @var string $name
     * @ORM\Column(type="string", length=100)
     */
    public $name;
    /**
     * @var string $description
     * @ORM\Column(type="text")
     */
    public $description;
    /**
     * @var \DateTime $created
     * @ORM\Column(type="datetime")
     */
    public $created;
    /**
     * @var \DateTime $updated
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $updated;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Document
     */
    public function setId(string $id): Document
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Document
     */
    public function setName(string $name): Document
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Document
     */
    public function setDescription(string $description): Document
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return Document
     */
    public function setCreated(\DateTime $created): Document
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     * @return Document
     */
    public function setUpdated(\DateTime $updated): Document
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated = new \DateTime("now");
    }
}