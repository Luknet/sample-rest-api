<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 06.10.2018
 * Time: 17:44
 */

namespace App\Controller;


use App\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;


/**
 * Class DocumentController
 * @package App\Controller
 * @Route("/document")
 */
class DocumentController extends Controller
{
    /**
     * @FOSRest\Get("/");
     */
    public function getDocumentsAction(): View
    {

        $repository = $this->getDoctrine()->getRepository(Document::class);

        $document = $repository->findall();

        return View::create($document, Response::HTTP_OK, []);
    }

    /**
     * @param string $id
     * @FOSRest\Get("/{id}");
     */
    public function getDocumentAction(string $id): View
    {
        $repository = $this->getDoctrine()->getRepository(Document::class);

        $document = $repository->find($id);
        if (!$document) {
            return View::create($document, Response::HTTP_NOT_FOUND, []);
        }
        return View::create($document, Response::HTTP_OK, []);
    }

    /**
     * @param Request $request
     * @FOSRest\Post("/")
     */
    public function postDocumentAction(Request $request): View
    {
        $document = new Document();
        $document->setName($request->get('name'));
        $document->setDescription($request->get('description'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($document);
        $em->flush();
        return View::create($document, Response::HTTP_CREATED, []);
    }


}