<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 23.10.2018
 * Time: 21:31
 */

namespace App\RequestToArray;


class Raw extends AbstractRequestToArray implements RequestToArrayInterface
{
    /**
     * @param string $context
     * @return array
     */
    public function deserialize(string $context): array
    {
        return $this->serializer->toArray(unserialize($context));
    }
}