<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 23.10.2018
 * Time: 21:20
 */

namespace App\RequestToArray;


/**
 * Interface RequestToArrayInterface
 * @package App\RequestToArray
 */
interface RequestToArrayInterface
{
    /**
     * @param string $context
     * @return array
     */
    public function deserialize(string $context):array ;
}