<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 23.10.2018
 * Time: 21:26
 */

namespace App\RequestToArray;


use JMS\Serializer\SerializerBuilder;

/**
 * Class AbstractRequestToArray
 * @package App\RequestToArray
 */
abstract class AbstractRequestToArray
{
    /**
     * @var \JMS\Serializer\Serializer
     */
    protected $serializer;

    /**
     * AbstractRequestToArray constructor.
     */
    public function __construct()
    {
        $this->serializer = (new SerializerBuilder())->build();
    }
}