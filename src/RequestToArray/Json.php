<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 23.10.2018
 * Time: 21:24
 */

namespace App\RequestToArray;


class Json extends AbstractRequestToArray implements RequestToArrayInterface
{
    /**
     * @param string $context
     * @return array
     */
    public function deserialize(string $context): array
    {
        return $this->serializer->deserialize($context, 'array', 'json');
    }
}