<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 23.10.2018
 * Time: 21:33
 */

namespace App\RequestToArray;


/**
 * Class RequestToArrayFactory
 * @package App\RequestToArray
 */
class RequestToArrayFactory
{
    /**
     * @param string $type
     * @return RequestToArrayInterface
     */
    static public function get($type)
    {
        $instance = null;

        switch ($type) {
            case 'json':
                $instance = new Json();
                break;
            case 'xml':
                $instance = new Xml();
                break;
            case 'raw':
                $instance = new Raw();
                break;
            default:
                throw new \InvalidArgumentException();
        }

        return $instance;
    }
}