<?php
/**
 * Created by PhpStorm.
 * User: ljecz
 * Date: 16.10.2018
 * Time: 22:37
 */

namespace App\View;


use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RawViewHandler
{
    /**
     * @param ViewHandler $viewHandler
     * @param View        $view
     * @param Request     $request
     * @param string      $format
     *
     * @return Response
     */
    public function createResponse(ViewHandler $handler, View $view, Request $request, $format)
    {
        return new Response(serialize($view->getData()), 200, $view->getHeaders());
    }
}